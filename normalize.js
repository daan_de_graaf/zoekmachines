/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Note: this file is specific for this dataset.
 */

var synonyms = {
	// '(AZ)': 'Algemene Zaken',
	// 'Minister-President': 'Algemene Zaken',
	// '(MP)': 'Algemene Zaken',

	'(BZK)': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'(BVK)': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'(BIZA)': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'Binnenlandse Zaken': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'Binnenlandse Zaken (BIZA)': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'Koninkrijksrelaties': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'(GSI)': 'Binnenlandse Zaken en Koninkrijksrelaties',
	'Binnenlandse Zaken en Koninkrijksrelaties (BZK)': 'Binnenlandse Zaken en Koninkrijksrelaties',

	// 'Ontwikkelingssamenwerking': 'Buitenlandse Handel en Ontwikkelingssamenwerking',
	// '(OS)': 'Buitenlandse Handel en Ontwikkelingssamenwerking',
	// '(OSW)': 'Buitenlandse Handel en Ontwikkelingssamenwerking',

	'Buitenlandse Zaken (BUZA)': 'Buitenlandse Zaken',
	'Buitenlandse Zaken Ontwikkelingssamenwerking': 'Buitenlandse Zaken',
	'Ontwikkelingssamenwerking': 'Buitenlandse Zaken',
	'Ontwikkelingssamenwerking (OSW)': 'Buitenlandse Zaken',
	'Ontwikkelingssamenwerking (OS)': 'Buitenlandse Zaken',

	// '(DEF)': 'Defensie',

	'Economische Zaken (EZ)': 'Economische Zaken',
	'Landbouw, Natuurbeheer en Visserij': 'Economische Zaken',
	'(LNV)': 'Economische Zaken',
	'Visserij': 'Economische Zaken',
	'Landbouw, Natuurbeheer en Visserij (LNV)': 'Economische Zaken',

	'Financiën (FIN)': 'Financiën',
	'Financien': 'Financiën',
	'FinanciÃ«n': 'Financiën',
	'FinanciÃ«n (FIN)': 'Financiën',

	'(VROM)': 'Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer',
	'Milieubeheer': 'Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer',

	'(OCW)': 'Onderwijs, Cultuur en Wetenschap',
	'Wetenschappen': 'Onderwijs, Cultuur en Wetenschap',
	'Onderwijs, Cultuur en Wetenschappen': 'Onderwijs, Cultuur en Wetenschap',
	'Onderwijs, Cultuur en Wetenschappen (OCW)': 'Onderwijs, Cultuur en Wetenschap',

	'Sociale Zaken en Werkgelegenheid (SZW)': 'Sociale Zaken en Werkgelegenheid',
	'Werkgelegenheid': 'Sociale Zaken en Werkgelegenheid',

	'Justitie (JUS)': 'Veiligheid en Justitie',
	'Justitie': 'Veiligheid en Justitie',
	'(JUS)': 'Veiligheid en Justitie',
	'(VI)': 'Veiligheid en Justitie',
	'(VZI)': 'Veiligheid en Justitie',
	'Vreemdelingenzaken en Integratie (VI)': 'Veiligheid en Justitie',

	'Verkeer en Waterstaat (VW)': 'Verkeer en Waterstaat',
	'Waterstaat': 'Verkeer en Waterstaat',
	'Verkeer en waterstaat': 'Verkeer en Waterstaat',

	'Sport': 'Volksgezondheid, Welzijn en Sport',
	'Volksgezondheid, Welzijn en Sport (VWS)': 'Volksgezondheid, Welzijn en Sport'
};

var normalize = function normalize(string) {
	for (var key in synonyms) {
		/*if (string.indexOf(key) >= 0)
			return synonyms[key];
		else if (string.indexOf(synonyms[key]) >= 0)
			return synonyms[key];
		else*/ if (synonyms[key] == string)
			return string;
		else if (string in synonyms)
			return synonyms[string];
	}

	// if (string.length < 50)
		// console.log("Missing:", string);

	return null;
};

module.exports = normalize;