/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Stemmer = null;

var Tokenizer = function Tokenizer() {

};

Tokenizer.tokenize = function tokenize(text, stem) {
	/* Start by replacing all but alphanumerical characters and whitespace. */
	text = text.replace(/[^a-zA-Z0-9\s]/g, ' ');

	/* Split words by space. */
	var tokens = text.split(' ');

	/* Remove "empty" tokens. */
	tokens = tokens.filter(function(x) {
		return !!x.length;
	});

	/* Lowercase all tokens. */
	tokens = tokens.map(function(x) {
		return x.toLowerCase();
	});

	/* Filter short words. */
	// tokens = tokens.filter(function(x) {
	// 	return x.length >= 20;
	// });

	if (stem) {
		if (Stemmer == null)
			Stemmer = require('./stemmer');
		
		tokens = tokens.map(function(x) {
			return Stemmer.stem(x);
		});
	}

	return tokens;
};

module.exports = Tokenizer;
