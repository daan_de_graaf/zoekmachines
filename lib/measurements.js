/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Measurements = function() {
	this.tp = {};
	this.tn = {};
	this.fp = {};
	this.fn = {};
};

Measurements.prototype.true = function t(klass, type) {
	if (type == 'p')
		this.tp[klass] = (this.tp[klass] || 0) + 1;
	else
		this.tn[klass] = (this.tn[klass] || 0) + 1;
};

Measurements.prototype.false = function f(klass, type) {
	if (type == 'p')
		this.fp[klass] = (this.fp[klass] || 0) + 1;
	else
		this.fn[klass] = (this.fn[klass] || 0) + 1;
};

Measurements.prototype.get = function result(klass) {
	var tp = this.tp[klass] || 0,
		fp = this.fp[klass] || 0,
		fn = this.fn[klass] || 0;

	if (tp == 0)
		return 0;

	return {
		precision: tp / (tp + fp),
		recall:    tp / (tp + fn),
	};
};

Measurements.prototype.all = function all() {
	var results = [];

	for (var klass in this.tp) {
		var result = this.get(klass);

		results.push([
			klass,
			result.precision,
			result.recall,
			/* F1: */
			2 * (result.precision * result.recall) /
			(result.precision + result.recall)
		]);
	}

	return results;
};

module.exports = Measurements;
