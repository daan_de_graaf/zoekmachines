/*
 * Copyright (C) 2015-10-15, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Probability = require('./probability');

var Distribution = function Distribution(outOfBounds) {
	this.outOfBounds = outOfBounds;

	this.samples     = {};
	this.sum         = 0;
};

Distribution.prototype.increment = function increment(event, delta) {
	var samples = this.samples;

	if (arguments.length >= 2 && typeof arguments[0] == 'string' &&
		typeof arguments[1] == 'string') {

		if (!(arguments[0] in samples))
			samples[arguments[0]] = new Distribution();

		this.sum += (arguments[2] || 1);

		return samples[arguments[0]].increment(arguments[1], arguments[2] || 1);
	}

	if (arguments.length == 1)
		delta = 1;

	samples[event] = (samples[event] || 0) + delta;
	this.sum += delta;
};

Distribution.prototype.draw = function draw(other) {
	for (var event in other) {
		if (other[event].constructor == Distribution) {
			for (var real in other[event].samples) {
				this.increment(event, real, other[event].samples[real]);
			}
		}else {
			this.increment(event, other[event]);
		}
	}
};

Distribution.prototype.concat = function concat(other) {
	var result = new Distribution(this.outOfBounds);

	result.draw(this.samples);
	result.draw(other.samples);

	return result;
};

/**
 * This function can be used to multiply an entire distribution by a given
 * function that returns a multiplication factor.
 * Note that this function also normalizes the distribution by updating the sum.
 */
Distribution.prototype.transform = function transform(func) {
	var result = new Distribution();

	for (var variable in this.samples) {
		for (var event in this.samples[variable].samples) {
			var value = this.samples[variable].samples[event];
			var factor = func(variable, event, value);
			value = factor;

			if (value == 0)
				continue;

			result.increment(variable, event, value);
		}
	}

	return result;
};

Distribution.prototype.sumEvent = function sumEvent(event) {
	var result = 0;

	for (var variable in this.samples) {
		if (event in this.samples[variable].samples)
			result += this.samples[variable].samples[event];
	}

	return result;
};

Distribution.prototype.countEvent = function countEvent(event) {
	var result = 0;

	for (var variable in this.samples) {
		if (event in this.samples[variable].samples)
			result += 1;
	}

	return result;
};

Distribution.prototype.cardinality = function cardinality(variable) {
	if (arguments.length == 1) {
		return this.samples[variable].cardinality();
	}
	
	return Object.keys(this.samples).length;
};

Distribution.prototype.probability = function probability(event) {
	if (arguments.length == 2)
		return this.samples[event].probability(arguments[1]);

	if (event in this.samples) {
		return new Probability(this.samples[event], this.sum);
	}

	return null;
};

Distribution.prototype.each = function each(func) {
	for (var event in this.samples)
		func(event, this.probability(event));
};

Distribution.prototype.variables = function variables() {
	return Object.keys(this.samples);
};

Distribution.prototype.probabilities = function probabilities(variable) {
	var results = [];

	for (var event in this.samples[variable].samples) {
		var prob = this.samples[variable].probability(event);
		results.push([event, prob]);
	}

	return results;
};

module.exports = Distribution;
