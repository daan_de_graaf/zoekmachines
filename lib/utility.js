/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Utility = function Utility() {

};

/**
 * This function calculates the Utility of the given term for the given klass,
 * by calculating the Mutual Information.
 *
 * The equation is a lot easier to understand than the description above though:
 * U = P(x, y) / (P(x) * P(y))
 */

Utility.mi = function mi(nb, klass, term) {
	var pxy = nb.tf.probability(klass, term);

	if (!pxy)
		return null;

	pxy.den = nb.tf.sum;
	pxy = pxy.float();
	var px  = nb.tf.sumEvent(term) / nb.tf.sum;
	var py  = nb.tf.samples[klass].sum / nb.tf.sum;

	return pxy / (px * py);
};

module.exports = Utility;
