/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Distribution = require('./distribution');

var Trainer = function Trainer(nb) {
	this.nb    = nb;

	this.prior = new Distribution();
	this.tf    = new Distribution();
};

Trainer.prototype.train = function train(tokens, klass) {
	/* Adjust the prior probabilities. */
	this.prior.increment(klass);

	/* Adjust the term frequencies. */
	tokens.forEach(function(token) {
		this.tf.increment(klass, token);
	}.bind(this));
}

Trainer.prototype.finish = function finish() {
	/* Concatenate the existing (possibly empty) term frequencies to the newly
	 * trained ones. Note that we need to concatenate PRIOR to (re)computing the
	 * IDFs. */
	this.nb.tf = this.nb.tf.concat(this.tf);

	/* Concatenate the existing (possibly empty) prior chances to the newly
	 * trained ones. */
	this.nb.prior = this.nb.prior.concat(this.prior);

	/* Now, compute the conditional probabilities. */
	this.nb.update();
};

module.exports = Trainer;
