/*
 * Copyright (C) 2015-10-15, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Distribution = require('./distribution'),
	Trainer      = require('./trainer'),
	Utility      = require('./utility');

var NB = function NB() {
	this.prior = new Distribution();
	this.tf    = new Distribution();

	/* Weighted Term Frequencies. The abbreviation is not pun intended. */
	this.wtf   = null;

	this.log_base = 2;

	/* I'm not sure why, but in order to get reasonable results, we need to
	 * use the log_base as exponent for the MutualInformation. */
	this.mi_power = this.log_base;
};

NB.prototype.update = function update() {
	this.wtf = this.tf.transform(function(klass, term, value) {
		return this.termWeighter(this, klass, term, value);
	}.bind(this));

	this.min = null;

	for (var klass in this.wtf.samples) {
		this.wtf.samples[klass].each(function(term, prob) {
			if (!this.min || this.min.compare(prob) > 0)
				this.min = prob;
		}.bind(this));
	}
};

NB.prototype.train = function train() {
	return new Trainer(this);
};

NB.prototype.klassify = function klassify(tokens) {
	var results = {};

	this.prior.each(function(klass, probability) {
		results[klass] = probability.log(this.log_base);

		tokens.forEach(function(token) {
			var prob = this.wtf.probability(klass, token);

			if (prob)
				results[klass] += prob.log(this.log_base);
			else
				results[klass] += this.min.log(this.log_base);
		}.bind(this));
	}.bind(this));

	var list = [];

	for (var key in results)
		list.push([key, results[key]]);

	return list.sort(function(a, b) {
		return b[1] - a[1];
	});
};

/**
 * This function returns the precomputed probability for the given event in the
 * given klass. Note that it DOES NOT include the a priori probability, nor does
 * it compute the logartihm of the returned probability.
 */
NB.prototype.probability = function probability(klass, event) {
	return this.wtf.probability(klass, event);
}

/**
 * This function returns `limit` terms with the highest tf-idf for the given
 * klass.
 */
NB.prototype.keywords = function keywords(klass, limit) {
	var results = this.wtf.probabilities(klass).sort(function(a, b) {
		return - a[1].compare(b[1]);
	});

	return results.slice(0, Math.min(limit || 5, results.length));
};

module.exports = NB;
