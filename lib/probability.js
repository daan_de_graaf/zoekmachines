/*
 * Copyright (C) 2015-10-15, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * This class is particulary useful prior to multiplying all probs with the IDF
 * because it's easier to determine what numbers the probability consists of.
 * E.g. if you have 23 classes and a word occurs in 3 of them, then 0.13
 * isn't going to be very helpful with debugging. 3/23 is.
 */

var Probability = function Probability(num, den) {
	this.num = num;
	this.den = den;
};

Probability.prototype.is = function is(num, den) {
	return (this.num == num && this.den == den);
};

Probability.prototype.compare = function compare(other) {
	var a = this.num / this.den,
		b = other.num / other.den;

	return a - b;
};

Probability.prototype.log = function log(base) {
	return Math.log(this.num / this.den) / Math.log(base);
};

Probability.prototype.float = function float() {
	return this.num / this.den;
};

module.exports = Probability;
