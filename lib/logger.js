/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Logger = function Logger(num, titles) {
	this.num      = num;
	this.current  = [];
	this.last     = [];
	this.titles   = titles;

	/* Note: getTime() returns **ms** since epoch. */
	this.start    = (new Date()).getTime();
};

Logger.prototype.percentage = function percentage(value) {
	var str = (value * 100).toFixed(2);

	while (str.length < 6)
		str = ' ' + str;

	return str;
};

Logger.prototype.progress = function pogress(index) {
	if (index) index --;
	else       index = 0;

	var i = (this.current[index] = (this.current[index] || 0) + 1);

	var fixed = (i / this.num * 100).toFixed(0);

	if (this.last[index] == fixed || index != 0)
		return;

	this.last[index] = fixed;

	var duration = ((new Date()).getTime() - this.start) / 1000;

	var all = [];

	this.titles.forEach(function(title, i) {
		var p = this.current[i];

		if (i) p /= this.current[0];
		else   p /= this.num;

		all.push([
			title + ':',
			this.percentage(p) + '%',
			'–'
		].join(' '));
	}.bind(this));

	console.log(all.join(' '), '(' + (i / duration).toFixed() + ' /sec)');
};

module.exports = Logger;
