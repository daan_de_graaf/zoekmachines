/*
 * Copyright (C) 2015-10-16, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var Utility = require('./utility');

var TermWeighter = function TermWeighter() {

};

TermWeighter.None = function None(nb, klass, term, value) {
	return value;
};

TermWeighter.InvertedDocumentFrequency =
function InvertedDocumentFrequency(nb, klass, term, value) {
	var idf = nb.prior.cardinality() / nb.tf.countEvent(term);

	/* Note that the multiplication is 0 when a term occurs in all classes. */
	return value * Math.log(idf, nb.log_base);
};

TermWeighter.MutualInformation =
function MutualInformation(nb, klass, term, value) {
	return value * Math.log(Utility.mi(nb, klass, term), nb.log_base);
};

TermWeighter.Both = function Both(nb, klass, term, value) {
	return TermWeighter.InvertedDocumentFrequency(nb, klass, term, value) *
		   TermWeighter.MutualInformation(nb, klass, term, value);
};

module.exports = TermWeighter;
