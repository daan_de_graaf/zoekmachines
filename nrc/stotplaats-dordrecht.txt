Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer

Op een stortplaats in Dordrecht is veel meer asbest aangetroffen dan verwacht.
Omwonenden klagen al jaren over gezondheidsklachten.

Rotterdam. Op stortplaats Derde Merwedehaven in Dordrecht, die dichtgaat en
wordt omgevormd tot recreatiegebied, zijn tienduizenden tonnen asbest meer
gestort dan voorheen werd aangenomen. Dat meldt de NOS op basis van documenten
die het in handen heeft. Provincie Zuid-Holland had berekend dat er 131.000 ton
aan asbesthoudend bouw- en sloopafval zou liggen. De NOS vroeg de provincie om
alle stortlijsten van 1993 tot en met 2003. Daaruit bleek dat de hoeveelheid
afval gebrekkig werd bijgehouden. Uit de lijsten vanaf 2003 blijkt dat er 45
duizend ton extra asbest kan worden bijgeteld aan de eerdere geschatte
hoeveelheid. Sinds januari 2011 wordt er overigens geen onverpakt asbesthoudend
afval meer gestort in de Derde Merwedehaven, meldt RTV Rijnmond. Een groot deel
van de omwonenden, onder wie inwoners van het aanliggende Sliedrecht klagen al
jaren over stank en gezondheidsklachten die volgens hen veroorzaakt worden door
de Dordtse stortplaats. In 2000 oordeelde het Rijksinstituut voor
Volksgezondheid en Milieu in een onderzoek dat hun lichamelijke problemen niet
door giftige stoffen werden veroorzaakt, maar door de zorgen die de omwonenden
van de stortplaats zich maken. NRC
