/*
 * Copyright (C) 2015-10-15, Daan de Graaf & Tim van Elsloo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var assert = require('assert');

var Logger       = require('./lib/logger'),
	NB           = require('./lib/nb'),
	Random       = require('./lib/random'),
	TermWeighter = require('./lib/term-weighter'),
	Tokenizer    = require('./lib/tokenizer'),
	Utility      = require('./lib/utility'),
	Measurements = require('./lib/measurements'),
	normalize    = require('./normalize');

console.log("Configuration:");
console.log("NoneWeighting:", process.argv.indexOf('--none') >= 0);
console.log("MutualInformation:", process.argv.indexOf('--mi') >= 0);
console.log("InvertedDocumentFrequency:", process.argv.indexOf('--mi') < 0 && process.argv.indexOf('--none') < 0);
console.log("Stemming:", process.argv.indexOf('--stem') >= 0);

var nb = new NB();

if (process.argv.indexOf('--mi') >= 0)
	nb.termWeighter = TermWeighter.MutualInformation;
else if (process.argv.indexOf('--none') >= 0)
	nb.termWeighter = TermWeighter.None;
else if (process.argv.indexOf('--both') == 0)
	nb.termWeighter = TermWeighter.Both;
else
	nb.termWeighter = TermWeighter.InvertedDocumentFrequency;

console.log("Reading file from disk!");

var docs = [];

require('readline').createInterface({
	input: require('fs').createReadStream(process.argv[2])
}).on('line', function(line) {
	var row = line.split('\t ');
	
	if (row.length <= 2)
		return;

	var klass = row[row.length - 1];
	klass = klass.replace(/^\s+/g, '').replace(/\s+$/g, '');

	if (!normalize(klass)) {
		klass = row[row.length - 2].replace(/^\s+/g, '').replace(/\s+$/g, '') +
				' ' + klass;

		if (!normalize(klass)) {
			// console.log('Missing', row[row.length - 1]);
			return;
		}
		
		row = row.slice(0, row.length - 1);
	}

	klass = normalize(klass);

	/* Make absolutely sure that we don't include the name of the ministry.
	 * Although I think it's IDF would render it useless anyway .. */
	row = row.slice(0, row.length - 1);

	docs.push([
		klass,
		Tokenizer.tokenize(
			row.join(' '),
			process.argv.indexOf('--stem') >= 0
		)
	]);

	if (docs.length % 1000 == 0)
		console.log("...", docs.length, "docs!");
}).on('close', function() {
	var trainer = nb.train();

	Random.subset(docs, 0.8 * docs.length, function(training, testing) {
		var logger = new Logger(training.length, ['Training']);

		training.forEach(function(doc, i) {
			logger.progress();

			trainer.train(doc[1], doc[0]);
		});

		trainer.finish();

		console.log('Prior:');

		for (var klass in trainer.prior.samples) {
			console.log(klass, trainer.prior.probability(klass).float());
		}

		logger = new Logger(testing.length, ['Testing', 'accuracy so far']);

		var m = new Measurements();

		testing.forEach(function(doc, i) {
			logger.progress(1);

			/* Only count the accuracy if the classification was right. */
			var result = nb.klassify(doc[1])[0][0];
			if (result == doc[0])
				logger.progress(2);

			if (result == doc[0])
				m.true(result, 'p');
			else {
				m.false(result, 'p');
				m.false(doc[0], 'n');
			}
		});

		console.log(m.all());
	});

	console.log("Utility of `de` in `Financiën`:", Utility.mi(trainer, 'Financiën', 'de'));
	console.log("Utility of `euro` in `Financiën`:", Utility.mi(trainer, 'Financiën', 'euro'));
	console.log("Utility of `weg` in `Financiën`:", Utility.mi(trainer, 'Financiën', 'weg'));
	
	console.log("Tf", nb.tf.probability('Veiligheid en Justitie', 'casino'));
	console.log("Top terms for biza", nb.keywords('Binnenlandse Zaken en Koninkrijksrelaties', 10));
	console.log("Top terms for buza", nb.keywords('Buitenlandse Zaken', 10));
	console.log("Top terms for ecoza", nb.keywords('Economische Zaken', 10));
	console.log("Top terms for fin", nb.keywords('Financiën', 10));
	console.log("Top terms for vrom", nb.keywords('Volkshuisvesting, Ruimtelijke Ordening en Milieubeheer', 10));
	console.log("Top terms for ocw", nb.keywords('Onderwijs, Cultuur en Wetenschap', 10));
	console.log("Top terms for szw", nb.keywords('Sociale Zaken en Werkgelegenheid', 10));
	console.log("Top terms for jus", nb.keywords('Veiligheid en Justitie', 10));
	console.log("Top terms for vw", nb.keywords('Verkeer en Waterstaat', 10));
	console.log("Top terms for vws", nb.keywords('Volksgezondheid, Welzijn en Sport', 10));
	console.log("Results:", nb.klassify(['de', 'school', 'waren', 'absoluut', 'niet', 'student'])[0]);

	if (process.argv.indexOf('--news') >= 0) {
		console.log("Testing news!");
		var news = require('fs').readdirSync('./nrc');
		news.forEach(function(file) {
			var data = require('fs').readFileSync('./nrc/' + file).toString();
			data = data.split('\n');

			var expected = data.splice(0, 1)[0],
				result   = nb.klassify(Tokenizer.tokenize(data.join(' ')))[0][0];

			console.log(file, result == expected, "– expected:", expected, "– got:", result);
		});
	}
});